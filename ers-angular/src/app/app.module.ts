import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginNavComponent } from './login-nav/login-nav.component';
import { LoggedInNavComponent } from './logged-in-nav/logged-in-nav.component';
import { LoggedInComponent } from './logged-in/logged-in.component';
import {HttpClientModule} from '@angular/common/http';
import { SubmitRequestComponent } from './submit-request/submit-request.component';
import { ViewRequestsEmployeeComponent } from './view-requests-employee/view-requests-employee.component';
import { ViewRequestsManagerComponent } from './view-requests-manager/view-requests-manager.component';
import { ViewEmployeesComponent } from './view-employees/view-employees.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginNavComponent,
    LoggedInNavComponent,
    LoggedInComponent,
    SubmitRequestComponent,
    ViewRequestsEmployeeComponent,
    ViewRequestsManagerComponent,
    ViewEmployeesComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      AppRoutingModule,
      HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
