package com.ers.controllers;

import com.ers.services.EmployeeService;
import com.ers.data.Employee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.muquit.libsodiumjna.SodiumLibrary;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Arrays;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



@WebServlet(description = "Account Information Servlet", urlPatterns = {"/account_info"})
public class AccountInfoServlet extends HttpServlet {
    

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	PrintWriter out = response.getWriter();
	ObjectMapper om = new ObjectMapper();
	
	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");
	if(account != null){
	    String strData = om.writeValueAsString(account);
	    out.write(strData);
	    response.setStatus(HttpServletResponse.SC_OK);
	    response.setHeader("content-type", "application/json");
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}
    }
}
