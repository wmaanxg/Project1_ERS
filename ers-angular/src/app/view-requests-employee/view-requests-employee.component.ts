import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';

import {Request} from '../domain/request';

@Component({
  selector: 'app-view-requests-employee',
  templateUrl: './view-requests-employee.component.html',
  styleUrls: ['./view-requests-employee.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ViewRequestsEmployeeComponent implements OnInit {

    requests: Request[];
    
    status: string;
    
    constructor(private router: Router,
		private route: ActivatedRoute,
		private http: HttpClient) { }
    
    ngOnInit() {
	this.getRequests();
    }

    getRequests(){
	this.http.get<Request[]>('http://localhost:8080/ERS/view_own_requests', {params: {'status': 'all'}, withCredentials: true})
	    .subscribe(
		data =>{
		    console.log(JSON.stringify(data));
		    this.requests = data;
		},
		err => {
		    this.router.navigateByUrl('/');
		});	
    }

    onSubmit(){
	this.http.get<Request[]>('http://localhost:8080/ERS/view_own_requests', {params: {'status': this.status}, withCredentials: true})
	    .subscribe(
		data =>{
		    this.requests = data;
		},
		err => {
		    this.router.navigateByUrl('/');
		});	
    }

}
