import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-logged-in-nav',
  templateUrl: './logged-in-nav.component.html',
  styleUrls: ['./logged-in-nav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoggedInNavComponent implements OnInit {

    name;
    
    constructor(private route: ActivatedRoute,
		private router: Router,
		private http: HttpClient) { }

    ngOnInit() {
	this.getAccount();
    }

    onLogout() {
	this.http.post('http://localhost:8080/ERS/logout', {}, {withCredentials: true})
	    .subscribe(
	    data =>{
		this.router.navigateByUrl('/');
	    },
	    err => {
		this.router.navigateByUrl('/');
	    });
    }
    
    getAccount() {
	this.http.get('http://localhost:8080/ERS/account_info', {withCredentials: true})
	    .subscribe(
		data =>{
		    this.name = data['firstname'];
		},
		err => {
		    this.router.navigateByUrl('/');
		});	
    }
    

}
