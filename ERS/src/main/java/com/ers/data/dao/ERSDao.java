package com.ers.data.dao;

import com.ers.exceptions.InvalidOptionException;

import java.io.Serializable;
import java.util.List;


public interface ERSDao<T, I extends Serializable> {
    List<T> getAll();
    T getOne(I id);
    void saveNew(T obj) throws InvalidOptionException;
    void update(T obj) throws InvalidOptionException;
    //void delete(I id);
    //void delete(T obj);
}
