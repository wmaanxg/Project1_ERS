import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';

import {Request} from '../domain/request'

@Component({
  selector: 'app-view-requests-manager',
  templateUrl: './view-requests-manager.component.html',
  styleUrls: ['./view-requests-manager.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ViewRequestsManagerComponent implements OnInit {

    requests: Request[];
    
    status: string;
    employee: string;
    
    constructor(private router: Router,
		private route: ActivatedRoute,
		private http: HttpClient) { }

    ngOnInit() {
	this.getRequests();
    }

    getRequests(){
	this.http.get<Request[]>('http://localhost:8080/ERS/view_all_requests', {params: {'status': 'all'}, withCredentials: true})
	    .subscribe(
		data =>{
		    console.log(JSON.stringify(data));
		    this.requests = data;
		    
		},
		err => {
		    this.router.navigate(['../logged_in'], {relativeTo: this.route});
		});	
    }

    onSubmit(){
	this.http.get<Request[]>('http://localhost:8080/ERS/view_all_requests', {params: {'employee': this.employee, 'status': this.status}, withCredentials: true})
	    .subscribe(
		data =>{
		    this.requests = data;

		},
		err => {
		    this.router.navigateByUrl('/');
		});	
    }

    onApprove(rId){
	this.http.post('http://localhost:8080/ERS/resolve_request', {status: 'approve', id: rId } ,{withCredentials: true})
	    .subscribe(
		data =>{
		    this.getRequests();
		},
		err => {
		    this.router.navigateByUrl('/');
		});	
    }
    onDeny(rId){
	this.http.post('http://localhost:8080/ERS/resolve_request', {'status': 'deny', 'id': rId } ,{withCredentials: true})
	    .subscribe(
		data =>{
		    this.getRequests();
		},
		err => {
		    this.router.navigateByUrl('/');
		});	
    }
    
}
