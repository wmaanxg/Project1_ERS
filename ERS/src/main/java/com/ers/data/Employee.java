package com.ers.data;

import com.ers.data.ReimbursementRequest;
import com.ers.data.ReimbursementRequest.ReimbursementRequestBuilder;
import com.ers.data.dao.ReimbursementRequestDao;
import com.ers.exceptions.InvalidOptionException;

import java.util.List;
import java.sql.Date;
import java.math.BigDecimal;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Employee{

    private final Integer id;
    private Integer ssn;
    private String email;
    private String firstname;
    private String lastname;
    private String title;
    private Boolean isManager;
    private Integer managerId;
    private Date birthdate;
    private String phone;
    private String stAddress;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    
    @JsonIgnore
    private String passwordHash;
    @JsonIgnore
    private String passwordResetRequired;

    private static final Logger logger = LogManager.getLogger(Employee.class.getName());
    
    public Employee(Employee e){
	this.id = e.id;
	this.ssn = e.ssn;
	this.email = e.email;
	this.firstname = e.firstname;
 	this.lastname = e.lastname;
	this.title = e.title;
	this.isManager = e.isManager;
	this.managerId = e.managerId;
	this.birthdate = e.birthdate;
	this.phone = e.phone;
	this.stAddress = e.stAddress;
	this.city = e.city;
	this.state = e.state;
	this.country = e.country;
	this.zipcode = e.zipcode;
	this.passwordHash = e.passwordHash;
	this.passwordResetRequired = e.passwordResetRequired;
    }

    private Employee(EmployeeBuilder builder){
	this.id = builder.id;
	this.ssn = builder.ssn;
	this.email = builder.email;
	this.firstname = builder.firstname;
 	this.lastname = builder.lastname;
	this.title = builder.title;
	this.isManager = builder.isManager;
	this.managerId = builder.managerId;
	this.birthdate = builder.birthdate;
	this.phone = builder.phone;
	this.stAddress = builder.stAddress;
	this.city = builder.city;
	this.state = builder.state;
	this.country = builder.country;
	this.zipcode = builder.zipcode;
	this.passwordHash = builder.passwordHash;
	this.passwordResetRequired = builder.passwordResetRequired;
    }


    public static class EmployeeBuilder{

	private Integer id;
	private Integer ssn;
	private String email;
	private String firstname;
	private String lastname;
	private String title;
	private Boolean isManager;
	private Integer managerId;
	private Date birthdate;
	private String phone;
	private String stAddress;
	private String city;
	private String state;
	private String country;
	private String zipcode;
	private String passwordHash;
	private String passwordResetRequired;

	public EmployeeBuilder(Integer id, Integer ssn,
			       String email,
			       String firstname, String lastname,
			       String title, Boolean isManager,
			       String passwordResetRequired){
	    this.id = id;
	    this.ssn = ssn;
	    this.email = email;
	    this.firstname = firstname;
	    this.lastname = lastname;
	    this.title = title;
	    this.isManager = isManager;
	    this.passwordResetRequired = passwordResetRequired;
	}



	public EmployeeBuilder setManagerId(Integer managerId){
	    this.managerId = managerId;
	    return this;
	}

	public EmployeeBuilder setBirthdate(Date birthdate){
	    this.birthdate = birthdate;
	    return this;
	}

	public EmployeeBuilder setPhone(String phone){
	    this.phone = phone;
	    return this;
	}

	public EmployeeBuilder setStAddress(String stAddress){
	    this.stAddress = stAddress;
	    return this;
	}

	public EmployeeBuilder setCity(String city){
	    this.city = city;
	    return this;
	}

	public EmployeeBuilder setState(String state){
	    this.state = state;
	    return this;
	}

	public EmployeeBuilder setCountry(String country){
	    this.country = country;
	    return this;
	}

	public EmployeeBuilder setZipcode(String zipcode){
	    this.zipcode = zipcode;
	    return this;
	}

	public EmployeeBuilder setPasswordHash(String passwordHash){
	    this.passwordHash = passwordHash;
	    return this;
	}


	public Employee build(){
	    return new Employee(this);
	}


    }

    // public static enum Title{

    // 	NONE(0, "invalid"),
    // 	EMPLOYEE(1, "employee"),
    // 	MANAGER(2, "manager");
	
    // 	private Integer id;
    // 	private String name;
    // 	//private Boolean isManager;
	
    // 	Title(Integer id, String name){
    // 	    this.id = id;
    // 	    this.name = name;
    // 	    //this.isManager = isManager;
    // 	}

    // 	public Integer getId(){
    // 	    return this.id;
    // 	}
    // 	public String getName(){
    // 	    return this.name;
    // 	}

    // 	public static Title getById(Integer id){
    // 	    Title ret = NONE;
    // 	    for(Title t : Title.values()){
    // 		if(t.id.compareTo(id) == 0){
    // 		    ret = t;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}

    // 	public static Title getByName(String name){
    // 	    Title ret = NONE;
    // 	    for(Title t : Title.values()){
    // 		if(t.name.compareTo(name) == 0){
    // 		    ret = t;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}
	
	
    // }

    // public static enum PasswordResetRequired{
    // 	NONE(0, "invalid"),
    // 	NO(1,"no"),
    // 	YES(2, "yes");

    // 	private Integer id;
    // 	private String name;

    // 	PasswordResetRequired(Integer id, String name){
    // 	    this.id = id;
    // 	    this.name = name;
    // 	}

    // 	public Integer getId(){
    // 	    return this.id;
    // 	}
    // 	public String getName(){
    // 	    return this.name;
    // 	}

	
    // 	public static PasswordResetRequired getById(Integer id){
    // 	    PasswordResetRequired ret = NONE;
    // 	    for(PasswordResetRequired p : PasswordResetRequired.values()){
    // 		if(p.id.compareTo(id) == 0){
    // 		    ret = p;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}

    // 	public static PasswordResetRequired getByName(String name){
    // 	    PasswordResetRequired ret = NONE;
    // 	    for(PasswordResetRequired p : PasswordResetRequired.values()){
    // 		if(p.name.compareTo(name) == 0){
    // 		    ret = p;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}	
    // }


    public void submitRequest(BigDecimal amount, String receipt, String type)
	throws InvalidOptionException{
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	ReimbursementRequestBuilder builder = new ReimbursementRequestBuilder(0, this.id, null, amount, type, "pending");
	ReimbursementRequest newRequest = builder.build();
	dao.saveNew(newRequest);
    }

    @JsonIgnore
    public List<ReimbursementRequest> getMyRequests(){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getByEmployee(this);
    }

    @JsonIgnore
    public List<ReimbursementRequest> getPendingRequests(){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getPendingBySubmitter(this);
    }

    @JsonIgnore
    public List<ReimbursementRequest> getResolvedRequests(){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getResolvedBySubmitter(this);
    }
    
    /**
     * Gets the value of id
     *
     * @return the value of id
     */
    public final Integer getId() {
	return this.id;
    }
    

    /**
     * Gets the value of ssn
     *
     * @return the value of ssn
     */
    public final Integer getSsn() {
	return this.ssn;
    }

    /**
     * Sets the value of ssn
     *
     * @param argSsn Value to assign to this.ssn
     */
    public final void setSsn(final Integer argSsn) {
	this.ssn = argSsn;
    }

    /**
     * Gets the value of email
     *
     * @return the value of email
     */
    public final String getEmail() {
	return this.email;
    }

    /**
     * Sets the value of email
     *
     * @param argEmail Value to assign to this.email
     */
    public final void setEmail(final String argEmail) {
	this.email = argEmail;
    }

    /**
     * Gets the value of firstname
     *
     * @return the value of firstname
     */
    public final String getFirstname() {
	return this.firstname;
    }

    /**
     * Sets the value of firstname
     *
     * @param argFirstname Value to assign to this.firstname
     */
    public final void setFirstname(final String argFirstname) {
	this.firstname = argFirstname;
    }

    /**
     * Gets the value of lastname
     *
     * @return the value of lastname
     */
    public final String getLastname() {
	return this.lastname;
    }

    /**
     * Sets the value of lastname
     *
     * @param argLastname Value to assign to this.lastname
     */
    public final void setLastname(final String argLastname) {
	this.lastname = argLastname;
    }

    /**
     * Gets the value of title
     *
     * @return the value of title
     */
    public final String getTitle() {
	return this.title;
    }

    /**
     * Sets the value of title
     *
     * @param argTitle Value to assign to this.title
     */
    public final void setTitle(final String argTitle) {
	this.title = argTitle;
    }

    /**
     * Gets the value of managerId
     *
     * @return the value of managerId
     */
    public final Integer getManagerId() {
	return this.managerId;
    }

    /**
     * Sets the value of managerId
     *
     * @param argManagerId Value to assign to this.managerId
     */
    public final void setManagerId(final Integer argManagerId) {
	this.managerId = argManagerId;
    }

    /**
     * Gets the value of birthdate
     *
     * @return the value of birthdate
     */
    public final Date getBirthdate() {
	return this.birthdate;
    }

    /**
     * Sets the value of birthdate
     *
     * @param argBirthdate Value to assign to this.birthdate
     */
    public final void setBirthdate(final Date argBirthdate) {
	this.birthdate = argBirthdate;
    }

    /**
     * Gets the value of phone
     *
     * @return the value of phone
     */
    public final String getPhone() {
	return this.phone;
    }

    /**
     * Sets the value of phone
     *
     * @param argPhone Value to assign to this.phone
     */
    public final void setPhone(final String argPhone) {
	this.phone = argPhone;
    }

    /**
     * Gets the value of stAddress
     *
     * @return the value of stAddress
     */
    public final String getStAddress() {
	return this.stAddress;
    }

    /**
     * Sets the value of stAddress
     *
     * @param argStAddress Value to assign to this.stAddress
     */
    public final void setStAddress(final String argStAddress) {
	this.stAddress = argStAddress;
    }

    /**
     * Gets the value of city
     *
     * @return the value of city
     */
    public final String getCity() {
	return this.city;
    }

    /**
     * Sets the value of city
     *
     * @param argCity Value to assign to this.city
     */
    public final void setCity(final String argCity) {
	this.city = argCity;
    }

    /**
     * Gets the value of state
     *
     * @return the value of state
     */
    public final String getState() {
	return this.state;
    }

    /**
     * Sets the value of state
     *
     * @param argState Value to assign to this.state
     */
    public final void setState(final String argState) {
	this.state = argState;
    }

    /**
     * Gets the value of country
     *
     * @return the value of country
     */
    public final String getCountry() {
	return this.country;
    }

    /**
     * Sets the value of country
     *
     * @param argCountry Value to assign to this.country
     */
    public final void setCountry(final String argCountry) {
	this.country = argCountry;
    }

    /**
     * Gets the value of zipcode
     *
     * @return the value of zipcode
     */
    public final String getZipcode() {
	return this.zipcode;
    }

    /**
     * Sets the value of zipcode
     *
     * @param argZipcode Value to assign to this.zipcode
     */
    public final void setZipcode(final String argZipcode) {
	this.zipcode = argZipcode;
    }

    
    /**
     * Gets the value of passwordHash
     *
     * @return the value of passwordHash
     */
    public final String getPasswordHash() {
	return this.passwordHash;
    }

    /**
     * Sets the value of passwordHash
     *
     * @param argPasswordHash Value to assign to this.passwordHash
     */
    public final void setPasswordHash(final String argPasswordHash) {
	this.passwordHash = argPasswordHash;
    }

    /**
     * Gets the value of passwordResetRequired
     *
     * @return the value of passwordResetRequired
     */
    public final String getPasswordResetRequired() {
	return this.passwordResetRequired;
    }

    /**
     * Sets the value of passwordResetRequired
     *
     * @param argPasswordResetRequired Value to assign to this.passwordResetRequired
     */
    public final void setPasswordResetRequired(final String argPasswordResetRequired) {
	this.passwordResetRequired = argPasswordResetRequired;
    }

    
    /**
     * Gets the value of isManager
     *
     * @return the value of isManager
     */
    public final Boolean getIsManager() {
	return this.isManager;
    }

    /**
     * Sets the value of isManager
     *
     * @param argIsManager Value to assign to this.isManager
     */
    public final void setIsManager(final Boolean argIsManager) {
	this.isManager = argIsManager;
    }


}
