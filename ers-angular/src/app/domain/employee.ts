export class Employee{
    id: number
    ssn: number;
    email: string;
    firstname: string;
    lastname: string;
    title: string;
    isManager: boolean;
    managerId: number;
    birthdate: string;
    phone: string;
    stAddress: string;
    city: string;
    state: string;
    country: string;
    zipcode: string;
}
