package com.ers.data;

import com.ers.data.ReimbursementRequest;
import com.ers.data.dao.ReimbursementRequestDao;
import com.ers.data.Employee;
import com.ers.data.dao.EmployeeDao;
import com.ers.exceptions.ForbiddenException;
import com.ers.exceptions.InvalidOptionException;

import java.util.List;

public class Manager extends Employee{

    public Manager(Employee e) throws ForbiddenException{
	super(e);
	if(!this.getIsManager())
	    throw new ForbiddenException();
    }

    public void approveRequest(ReimbursementRequest request)
	throws InvalidOptionException{
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	request.setStatus("approved");
	request.setResolverId(this.getId());
	dao.update(request);
    }

    public void denyRequest(ReimbursementRequest request)
	throws InvalidOptionException{
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	request.setStatus("denied");
	request.setResolverId(this.getId());
	dao.update(request);
    }

    public List<ReimbursementRequest> getAllRequests(){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getAll();	
    }
    
    public List<ReimbursementRequest> getAllPendingRequests(){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getPending();	
    }

    public List<ReimbursementRequest> getAllResolvedRequests(){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getResolved();	
    }

    public List<ReimbursementRequest> getRequestsByEmployee(Employee e){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getByEmployee(e);	
    }

    public List<ReimbursementRequest> getResolvedByEmployee(Employee e){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getResolvedBySubmitter(e);	
    }
    public List<ReimbursementRequest> getPendingByEmployee(Employee e){
	ReimbursementRequestDao dao = new ReimbursementRequestDao();
	return dao.getPendingBySubmitter(e);	
    }

    
    public List<Employee> getAllEmployees(){
	EmployeeDao dao = new EmployeeDao();
	return dao.getAll();	
    }    
    
}
