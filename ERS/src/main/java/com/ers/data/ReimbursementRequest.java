package com.ers.data;

import java.sql.Timestamp;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ReimbursementRequest{

    private final Integer id;    
    private final Integer submitterId;
    private Timestamp timeSubmitted;
    private BigDecimal amount;
    private String type;
    private String status;
    private String receipt;
    private Integer resolverId;

    private ReimbursementRequest(ReimbursementRequestBuilder builder){
	this.id = builder.id;
	this.submitterId = builder.submitterId;
	this.timeSubmitted = builder.timeSubmitted;
	this.amount = builder.amount;
	this.type = builder.type;
	this.status = builder.status;
	this.receipt = builder.receipt;
	this.resolverId = builder.resolverId;
    }

    public static class ReimbursementRequestBuilder{
	private Integer id;
	private Integer submitterId;
	private Timestamp timeSubmitted;
	private BigDecimal amount;
	private String type;
	private String status;
	private String receipt;
	private Integer resolverId;

	public ReimbursementRequestBuilder(Integer id, Integer submitterId,
					   Timestamp timeSubmitted,
					   BigDecimal amount,
					   String type, String status){
	    this.id = id;
	    this.submitterId = submitterId;
	    this.timeSubmitted = timeSubmitted;
	    this.amount = amount;
	    this.type = type;
	    this.status = status; 
	}

	public ReimbursementRequestBuilder setReceipt(String receipt){
	    this.receipt = receipt;
	    return this;
	}

	public ReimbursementRequestBuilder setResolverId(Integer resolverId){
	    this.resolverId = resolverId;
	    return this;
	}

	public ReimbursementRequest build(){
	    return new ReimbursementRequest(this);
	}
    }
    
    // public static enum Type{
    // 	NONE(0, "invalid"),
    // 	TRAINING(1,"training"),
    // 	TRAVEL(2, "travel"),
    // 	FOOD(3, "food"),
    // 	INCIDENTALS(4, "incidentals");

    // 	private Integer id;
    // 	private String name;

    // 	Type(Integer id, String name){
    // 	    this.id = id;
    // 	    this.name = name;
    // 	}

    // 	public Integer getId(){
    // 	    return this.id;
    // 	}
    // 	public String getName(){
    // 	    return this.name;
    // 	}

	
    // 	public static Type getById(Integer id){
    // 	    Type ret = NONE;
    // 	    for(Type t : Type.values()){
    // 		if(t.id.compareTo(id) == 0){
    // 		    ret = t;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}

    // 	public static Type getByName(String name){
    // 	    Type ret = NONE;
    // 	    for(Type t : Type.values()){
    // 		if(t.name.compareTo(name) == 0){
    // 		    ret = t;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}	
    // }
    // public static enum Status{
    // 	NONE(0, "invalid"),
    // 	PENDING(1,"pending"),
    // 	APPROVED(2, "approved"),
    // 	DENIED(3, "denied");
	
    // 	private Integer id;
    // 	private String name;

    // 	Status(Integer id, String name){
    // 	    this.id = id;
    // 	    this.name = name;
    // 	}

    // 	public Integer getId(){
    // 	    return this.id;
    // 	}
    // 	public String getName(){
    // 	    return this.name;
    // 	}

	
    // 	public static Status getById(Integer id){
    // 	    Status ret = NONE;
    // 	    for(Status s : Status.values()){
    // 		if(s.id.compareTo(id) == 0){
    // 		    ret = s;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}

    // 	public static Status getByName(String name){
    // 	    Status ret = NONE;
    // 	    for(Status s : Status.values()){
    // 		if(s.name.compareTo(name) == 0){
    // 		    ret = s;
    // 		}   
    // 	    }
    // 	    return ret;
    // 	}	
    // }

    
    /**
     * Gets the value of id
     *
     * @return the value of id
     */
    public final Integer getId() {
	return this.id;
    }


    /**
     * Gets the value of amount
     *
     * @return the value of amount
     */
    public final BigDecimal getAmount() {
	return this.amount;
    }

    /**
     * Sets the value of amount
     *
     * @param argAmount Value to assign to this.amount
     */
    public final void setAmount(final BigDecimal argAmount) {
	this.amount = argAmount;
    }

    /**
     * Gets the value of type
     *
     * @return the value of type
     */
    public final String getType() {
	return this.type;
    }

    /**
     * Sets the value of type
     *
     * @param argType Value to assign to this.type
     */
    public final void setType(final String argType) {
	this.type = argType;
    }

    /**
     * Gets the value of status
     *
     * @return the value of status
     */
    public final String getStatus() {
	return this.status;
    }

    /**
     * Sets the value of status
     *
     * @param argStatus Value to assign to this.status
     */
    public final void setStatus(final String argStatus) {
	this.status = argStatus;
    }

    /**
     * Gets the value of receipt
     *
     * @return the value of receipt
     */
    public final String getReceipt() {
	return this.receipt;
    }

    /**
     * Sets the value of receipt
     *
     * @param argReceipt Value to assign to this.receipt
     */
    public final void setReceipt(final String argReceipt) {
	this.receipt = argReceipt;
    }

    
    /**
     * Gets the value of submitterId
     *
     * @return the value of submitterId
     */
    public final Integer getSubmitterId() {
	return this.submitterId;
    }

    /**
     * Gets the value of timeSubmitted
     *
     * @return the value of timeSubmitted
     */
    public final Timestamp getTimeSubmitted() {
	return this.timeSubmitted;
    }

    /**
     * Sets the value of timeSubmitted
     *
     * @param argTimeSubmitted Value to assign to this.timeSubmitted
     */
    public final void setTimeSubmitted(final Timestamp argTimeSubmitted) {
	this.timeSubmitted = argTimeSubmitted;
    }

    /**
     * Gets the value of resolverId
     *
     * @return the value of resolverId
     */
    public final Integer getResolverId() {
	return this.resolverId;
    }

    /**
     * Sets the value of resolverId
     *
     * @param argResolverId Value to assign to this.resolverId
     */
    public final void setResolverId(final Integer argResolverId) {
	this.resolverId = argResolverId;
    }

}
