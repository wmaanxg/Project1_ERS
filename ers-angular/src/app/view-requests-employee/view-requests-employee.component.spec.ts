import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRequestsEmployeeComponent } from './view-requests-employee.component';

describe('ViewRequestsEmployeeComponent', () => {
  let component: ViewRequestsEmployeeComponent;
  let fixture: ComponentFixture<ViewRequestsEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRequestsEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRequestsEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
