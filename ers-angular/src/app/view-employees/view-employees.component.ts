import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

import {Employee} from '../domain/employee';

@Component({
  selector: 'app-view-employees',
  templateUrl: './view-employees.component.html',
  styleUrls: ['./view-employees.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ViewEmployeesComponent implements OnInit {

    employees: Employee[];
    
    constructor(private router: Router,
		private route: ActivatedRoute,
		private http: HttpClient) { }

    ngOnInit() {
	this.getEmployees();
    }

    getEmployees(){
	this.http.get<Employee[]>('http://localhost:8080/ERS/view_all_employees', {withCredentials: true})
	    .subscribe(
		data =>{
		    this.employees = data;
		    console.log(JSON.stringify(data));
		    console.log(JSON.stringify(this.employees));
		},
		err => {
		    this.router.navigate(['../logged_in'], {relativeTo: this.route});
		});
    }

}
