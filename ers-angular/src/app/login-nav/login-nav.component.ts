import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router'
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login-nav',
  templateUrl: './login-nav.component.html',
  styleUrls: ['./login-nav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginNavComponent implements OnInit {

    username: string = '';
    password: string = '';
    error: string = '';

    
    constructor(private route: ActivatedRoute,
		private router: Router,
		private http: HttpClient){
    }

    onSubmit(){
	var loginObj = {'username': this.username, 'password': this.password};
	this.http.post('http://localhost:8080/ERS/login_auth', loginObj, {responseType: 'text', withCredentials: true})
	    .subscribe(
		data =>{
		    console.log('logging in');
		    this.router.navigateByUrl('/logged_in(nav_outlet:logged_in_nav)');
		    //this.router.navigate([{ outlets: { nav_outlet: 'logged_in_nav'}}]);
		},
		err => {
		    this.error = 'Invalid login information';
		    console.log('Invalid login information');
		});
	
    }


    ngOnInit() {
    }

}
