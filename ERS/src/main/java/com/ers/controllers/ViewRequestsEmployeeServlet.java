package com.ers.controllers;

import com.ers.services.EmployeeService;
import com.ers.data.Employee;
import com.ers.exceptions.InvalidOptionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.muquit.libsodiumjna.SodiumLibrary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(description = "View Own Requests Servlet", urlPatterns = {"/view_own_requests"})
public class ViewRequestsEmployeeServlet extends HttpServlet {
    

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");
	ObjectMapper om = new ObjectMapper();
	PrintWriter writer = response.getWriter();
	
	if(account != null){
	    String requestStatus = request.getParameter("status");
	    
	    List requests = null;
	    String strData = null;
	    
	    switch(requestStatus){
	    case "all":
		requests = account.getMyRequests();
		break;
	    case "pending":
		requests = account.getPendingRequests();
		break;
	    case "resolved":	
		requests = account.getResolvedRequests();
		break;
	    default:
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return;
	    }
	    strData = om.writeValueAsString(requests);
	    writer.write(strData);
	    response.setStatus(HttpServletResponse.SC_OK);
	    response.setHeader("content-type", "appliecation/json");
	
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}
    }

}
