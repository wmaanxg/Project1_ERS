package com.ers.data.dao;

import com.ers.dbutils.ConnectionUtil;
import com.ers.data.ReimbursementRequest;
import com.ers.data.ReimbursementRequest.ReimbursementRequestBuilder;
//import com.ers.data.ReimbursementRequest.Type;
//import com.ers.data.ReimbursementRequest.Status;
import com.ers.data.Employee;
import com.ers.exceptions.InvalidOptionException;

import java.util.List;
import java.util.ArrayList;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.sql.Types;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class ReimbursementRequestDao implements ERSDao<ReimbursementRequest,
							   Integer>{

    private static final Logger logger = LogManager.getLogger(ReimbursementRequestDao.class.getName());

    private static final String selectAllFields = "SELECT r.id, r.submitter_id, r.time_submitted, r.amount, t.name, s.name, r.receipt, r.resolver_id FROM reimbursement_request r JOIN reimbursement_type t ON r.type_id = t.id JOIN reimbursement_status s ON r.status_id = s.id";
    
    @Override
    public List<ReimbursementRequest> getAll(){
	List<ReimbursementRequest> allRequests = new ArrayList<>();

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields;
	    try(Statement s = c.createStatement();
		ResultSet results = s.executeQuery(sql);){
	    
		while(results.next()){
		    allRequests.add(rowToReimbursementRequest(results));
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.getAll()");
	    logger.error(e.toString());
	}

	return allRequests;
    }

    @Override
    public ReimbursementRequest getOne(Integer id){
	ReimbursementRequest Request = null;

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE r.id = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setInt(1, id);
		try(ResultSet results = ps.executeQuery()){
		    while(results.next()){
			Request = rowToReimbursementRequest(results);
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.getOne()");
	    logger.error(e.toString());
	}
	return Request;
    }

    
    public List<ReimbursementRequest> getByEmployee(Employee e){
	List<ReimbursementRequest> requests = new ArrayList<>();

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE r.submitter_id = ?";
	    try(PreparedStatement s = c.prepareStatement(sql)){
		s.setInt(1, e.getId());
		try(ResultSet results = s.executeQuery()){   
		    while(results.next()){
			requests.add(rowToReimbursementRequest(results));
		    }
		}
		c.commit();
	    } catch(SQLException ex){
		c.rollback();
		throw ex;
	    }
	} catch (SQLException ex){
	    logger.error("SQLException in ReimbursementRequestDao.getByEmployee()");
	    logger.error(ex.toString());
	}

	return requests;
    }

    
    public List<ReimbursementRequest> getPending(){
	List<ReimbursementRequest> requests = new ArrayList<>();

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE s.name = ?";
	    try(PreparedStatement s = c.prepareStatement(sql)){
		s.setString(1, "pending");
		try(ResultSet results = s.executeQuery()){
		    while(results.next()){
			requests.add(rowToReimbursementRequest(results));
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.getPending()");
	    logger.error(e.toString());
	}

	return requests;
    }

    public List<ReimbursementRequest> getResolved(){
	List<ReimbursementRequest> requests = new ArrayList<>();

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE NOT s.name = ?";
	    try(PreparedStatement s = c.prepareStatement(sql)){
		s.setString(1, "pending");
		try(ResultSet results = s.executeQuery()){
		    while(results.next()){
			requests.add(rowToReimbursementRequest(results));
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.getResolved()");
	    logger.error(e.toString());
	}

	return requests;
    }
    
    public List<ReimbursementRequest> getPendingBySubmitter(Employee e){

	List<ReimbursementRequest> requests = new ArrayList<>();

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE r.submitter_id = ? AND s.name = ?";
	    try(PreparedStatement s = c.prepareStatement(sql)){
		s.setInt(1, e.getId());
		s.setString(2, "pending");
		try(ResultSet results = s.executeQuery()){
		    while(results.next()){
			requests.add(rowToReimbursementRequest(results));
		    }
		}
		c.commit();
	    } catch(SQLException ex){
		c.rollback();
		throw ex;
	    }
	} catch (SQLException ex){
	    logger.error("SQLException in ReimbursementRequestDao.getPendingBySubmitter()");
	    logger.error(ex.toString());
	}

	return requests;
    }

    
    public List<ReimbursementRequest> getResolvedBySubmitter(Employee e){
	
	List<ReimbursementRequest> requests = new ArrayList<>();

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE r.submitter_id = ? AND NOT s.name = ?";
	    try(PreparedStatement s = c.prepareStatement(sql)){
		s.setInt(1, e.getId());
		s.setString(2, "pending");
		try(ResultSet results = s.executeQuery()){
		    while(results.next()){
			requests.add(rowToReimbursementRequest(results));
		    }
		}
		c.commit();
	    } catch(SQLException ex){
		c.rollback();
		throw ex;
	    }
	} catch (SQLException ex){
	    logger.error("SQLException in ReimbursementRequestDao.getResolvedBySubmitter()");
	    logger.error(ex.toString());
	}

	return requests;
    }

    
    private ReimbursementRequest rowToReimbursementRequest(ResultSet row)
	throws SQLException{
	ReimbursementRequestBuilder builder =
	    new ReimbursementRequestBuilder(row.getInt(1),
					    row.getInt(2),
					    row.getTimestamp(3),
					    row.getBigDecimal(4),
					    row.getString(5),
					    row.getString(6))
	    .setReceipt(row.getString(7))
	    .setResolverId(row.getInt(8));
	return builder.build();
    }

    public void saveNew(ReimbursementRequest r) throws InvalidOptionException{
	
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "INSERT INTO reimbursement_request (submitter_id, amount, receipt, resolver_id, type_id, status_id) VALUES (?, ?, ?, ?, ?, ?)";

	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setInt(1, r.getSubmitterId());
		ps.setBigDecimal(2, r.getAmount());
		ps.setString(3, r.getReceipt());
		if(r.getResolverId() == null)
		    ps.setNull(4, Types.INTEGER);
		else
		    ps.setInt(4, r.getResolverId());
		ps.setInt(5, this.typeNameToId(r.getType()));
		ps.setInt(6, this.statusNameToId(r.getStatus()));

		ps.executeUpdate();
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.update()");
	    logger.error(e.toString());
	}
	
    }
    public void update(ReimbursementRequest r) throws InvalidOptionException{

	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "UPDATE reimbursement_request SET amount = ?, receipt = ?, resolver_id = ?, type_id = ?, status_id = ? WHERE id = ?";
	    
	    try(PreparedStatement ps = c.prepareStatement(sql)){
	    
		ps.setBigDecimal(1, r.getAmount());
		ps.setString(2, r.getReceipt());
		ps.setInt(3, r.getResolverId());
		ps.setInt(4, this.typeNameToId(r.getType()));
		ps.setInt(5, this.statusNameToId(r.getStatus()));
		ps.setInt(6, r.getId());

		ps.executeUpdate();
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.saveNew()");
	    logger.error(e.toString());
	}	
    }

    public Integer typeNameToId(String name) throws InvalidOptionException{
	
	Integer ret = null;
   
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "SELECT id FROM reimbursement_type" +
		" WHERE name = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setString(1, name);
		try(ResultSet results = ps.executeQuery()){
		    while(results.next()){
			ret = results.getInt(1);
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch(SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.typeNametoId()");
	    logger.error(e.toString());
	}

	if(ret == null){
	    throw new InvalidOptionException("Not a valid reimbursement request type name.");
	}
	return ret;
    }
    
    // public String typeIdToName(Integer id) throws InvalidOptionException{
    // 	String ret = null;
   
    // 	try(Connection c = ConnectionUtil.newConnection()){
    // 	    String sql = "SELECT name FROM reimbursement_request_type" +
    // 		" WHERE id = ?;";
    // 	    PreparedStatement ps = c.prepareStatement(sql);
    // 	    ps.setInt(1, id);
    // 	    ResultSet results = ps.executeQuery();

    // 	    while(results.next()){
    // 		ret = results.getString(1);
    // 	    }
	    
    // 	} catch(SQLException e){
    // 	    logger.error("SQLException in ReimbursementRequestDao.typeIdToName()");
    // 	    logger.error(e.toString());
    // 	}

    // 	if(ret == null){
    // 	    throw new InvalidOptionException("Not a valid reimbursement request type id.");
    // 	}
    // 	return ret;
    // }

    public Integer statusNameToId(String name) throws InvalidOptionException{

	Integer ret = null;
   
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "SELECT id FROM reimbursement_status" +
		" WHERE name = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setString(1, name);
		try(ResultSet results = ps.executeQuery()){
		    while(results.next()){
			ret = results.getInt(1);
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch(SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.statusNametoId()");
	    logger.error(e.toString());
	}

	if(ret == null){
	    throw new InvalidOptionException("Not a valid reimbursement request status name.");
	}
	return ret;
    }
    
    // public String statusIdToName(Integer id) throws InvalidOptionException{
    // 	String ret = null;
   
    // 	try(Connection c = ConnectionUtil.newConnection()){
    // 	    String sql = "SELECT name FROM reimbursement_request_status" +
    // 		" WHERE id = ?;";
    // 	    PreparedStatement ps = c.prepareStatement(sql);
    // 	    ps.setInt(1, id);
    // 	    ResultSet results = ps.executeQuery();

    // 	    while(results.next()){
    // 		ret = results.getString(1);
    // 	    }
	    
    // 	} catch(SQLException e){
    // 	    logger.error("SQLException in ReimbursementRequestDao.statusIdToName()");
    // 	    logger.error(e.toString());
    // 	}

    // 	if(ret == null){
    // 	    throw new InvalidOptionException("Not a valid reimbursement request status id.");
    // 	}
    // 	return ret;
    // }


    
}
