import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginNavComponent} from './login-nav/login-nav.component';
import {LoggedInNavComponent} from './logged-in-nav/logged-in-nav.component';
import {LoggedInComponent} from './logged-in/logged-in.component';
import {SubmitRequestComponent} from './submit-request/submit-request.component';
import {ViewRequestsEmployeeComponent} from './view-requests-employee/view-requests-employee.component';
import {ViewRequestsManagerComponent} from './view-requests-manager/view-requests-manager.component';
import {ViewEmployeesComponent} from './view-employees/view-employees.component'

const appRoutes: Routes = [
    {path: 'login', component: LoginNavComponent, outlet: 'nav_outlet'},
    {path: 'logged_in_nav', component: LoggedInNavComponent, outlet: 'nav_outlet'},
    {path: 'logged_in', component: LoggedInComponent},
    {path: 'submit_request', component: SubmitRequestComponent},
    {path: 'view_own_requests', component: ViewRequestsEmployeeComponent},
    {path: 'view_others_requests', component: ViewRequestsManagerComponent},
    {path: 'view_employees', component: ViewEmployeesComponent},
    {path: '', redirectTo: '/(nav_outlet:login)', pathMatch:'full'}
];

@NgModule({
    imports: [
	RouterModule.forRoot(appRoutes)
    ],
    exports: [
	RouterModule
    ]
})
export class AppRoutingModule { }
