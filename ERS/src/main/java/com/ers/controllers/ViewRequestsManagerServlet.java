package com.ers.controllers;

import com.ers.services.EmployeeService;
import com.ers.data.Employee;
import com.ers.data.Manager;
import com.ers.exceptions.InvalidOptionException;
import com.ers.exceptions.ForbiddenException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(description = "Manager view all requests servlet", urlPatterns = {"/view_all_requests"})
public class ViewRequestsManagerServlet extends HttpServlet {
    

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");
	
	if(account != null){
	    Manager mAccount = null;
	    try{
		mAccount = new Manager(account);
	    } catch(ForbiddenException e){
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		return;
	    }
	    ObjectMapper om = new ObjectMapper();
	    PrintWriter writer = response.getWriter();


	    String requestStatus = request.getParameter("status");
	    String employeeUsername = request.getParameter("employee");

	    EmployeeService service = new EmployeeService();
	    Employee e = service.getOne(employeeUsername);
	    List requests = null;
	    String strData = null;
	    
	    switch(requestStatus){
	    case "all":
		if(e != null)
		    requests = mAccount.getRequestsByEmployee(e);
		else
		    requests = mAccount.getAllRequests();
		break;
	    case "pending":
		if(e != null)
		    requests = mAccount.getPendingByEmployee(e);
		else
		    requests = mAccount.getAllPendingRequests();
		break;
	    case "resolved":
		if(e != null)
		    requests = mAccount.getResolvedByEmployee(e);
		else
		    requests = mAccount.getAllResolvedRequests();
		break;
	    default:	
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return;
	    }
	    strData = om.writeValueAsString(requests);
	    writer.write(strData);
	    response.setStatus(HttpServletResponse.SC_OK);
	    response.setHeader("content-type", "appliecation/json");
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}
    }

}
