package com.ers.services;

import com.ers.data.dao.EmployeeDao;
import com.ers.data.Employee;
import com.ers.exceptions.InvalidOptionException;

import java.util.List;

public class EmployeeService{
    private EmployeeDao dao;

    public EmployeeService(){
	this.dao = new EmployeeDao();
    }

    public List<Employee> getAll(){
	return this.dao.getAll();
    }

    public Employee getOne(Integer id){
	return this.dao.getOne(id);
    }

    public Employee getOne(String email){
	return this.dao.getOne(email);
    }

    
    public void saveNew(Employee e) throws InvalidOptionException{
	this.dao.saveNew(e);
    }

    public void update(Employee e) throws InvalidOptionException{
	this.dao.update(e);
    }

    public boolean emailExists(String email){
	return this.dao.emailExists(email);
    }
    
}
