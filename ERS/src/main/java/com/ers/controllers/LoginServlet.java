package com.ers.controllers;

import com.ers.services.EmployeeService;
import com.ers.data.Employee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.muquit.libsodiumjna.SodiumLibrary;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ers.dbutils.ConnectionUtil;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@WebServlet(description = "Login Authentication Servlet", urlPatterns = {"/login_auth"})
public class LoginServlet extends HttpServlet {
    // 	private static final long serialVersionUID = 1L;
    // 	public static final String HTML_START="<html><body>";
    // 	public static final String HTML_END="</body></html>";
       
    // /**
    //  * @see HttpServlet#HttpServlet()
    //  */
    // public LoginServlet() {
    //     super();
    //     // TODO Auto-generated constructor stub
    // }

    // /**
    //  * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
    //  */
    // protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // 	PrintWriter out = response.getWriter();
    // 	Date date = new Date();
    // 	out.println(HTML_START + "<h2>Hi There!</h2><br/><h3>Date="+date +"</h3>"+HTML_END);
    // 	try{
    // 	    String libraryPath = "/usr/lib/x86_64-linux-gnu/libsodium.so.18";
    // 	    SodiumLibrary.setLibraryPath(libraryPath);

    // 	    Connection c = ConnectionUtil.newConnection();
    // 	    String sql = "UPDATE employee SET password_hash = ? WHERE email = 'anonymous@gmail.com'";
    // 	    PreparedStatement s = c.prepareStatement(sql);
    // 	    s.setString(1, SodiumLibrary.cryptoPwhashStr("anonymous".getBytes()));
    // 	    s.executeUpdate();

    // 	    sql = "UPDATE employee SET password_hash = ? WHERE email = 'girugamesh.king@gmail.com'";
    // 	    s = c.prepareStatement(sql);
    // 	    s.setString(1, SodiumLibrary.cryptoPwhashStr("oregaruruda".getBytes()));
    // 	    s.executeUpdate();

    // 	    //c.commit();
    // 	} catch(Exception e){
    // 	    System.out.println(e);
    // 	}
    // }

    public static class LoginInfo{
	public String username;
	public String password;
	
	public LoginInfo(){}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	//Linux
	String libraryPath = "/usr/lib/x86_64-linux-gnu/libsodium.so.18";
	SodiumLibrary.setLibraryPath(libraryPath);

	PrintWriter writer = response.getWriter();
	EmployeeService es = new EmployeeService();
	ObjectMapper om = new ObjectMapper();
	
	try(BufferedReader reader = request.getReader();) {
	    LoginInfo user = om.readValue(reader, LoginInfo.class);
	    Employee account = es.getOne(user.username);
	    String hash = account.getPasswordHash();
	    byte[] pass = user.password.getBytes();

	    if(SodiumLibrary.cryptoPwhashStrVerify(hash, pass)){
		HttpSession session = request.getSession();
		session.setAttribute("account", account);
		response.setStatus(HttpServletResponse.SC_OK);
		response.setHeader("content-type", "text/plain");
	    }
	    else{
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.setHeader("WWW-Authenticate", "html-form");
	    }
	    
	} catch(JsonProcessingException e){
	    System.out.println(e);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}
    }


}
