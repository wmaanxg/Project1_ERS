package com.ers.controllers;

import com.ers.services.EmployeeService;
import com.ers.data.Employee;
import com.ers.exceptions.InvalidOptionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;


import java.io.IOException;
import java.io.BufferedReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(description = "Submit Request Servlet", urlPatterns = {"/submit_request"})
public class SubmitRequestServlet extends HttpServlet {
    

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");
	if(account != null){
	    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}
    }


    public static class RequestSubmission{

	public String amount;
	public String type;
	
	public RequestSubmission(){
	}
    }
    
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");

	if(account != null){
	    ObjectMapper om = new ObjectMapper();
	
	    try(BufferedReader reader = request.getReader();) {
		RequestSubmission submission = om.readValue(reader, RequestSubmission.class);
		Scanner sc = new Scanner(submission.amount);
		BigDecimal amount = sc.nextBigDecimal().setScale(2);
		String type = submission.type;

		account.submitRequest(amount, null, type);
		response.setStatus(HttpServletResponse.SC_CREATED);
		response.setHeader("content-type", "text/plain");
		
	    } catch(InvalidOptionException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(JsonProcessingException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(ArithmeticException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(InputMismatchException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(NoSuchElementException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    }
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}


    }

}
