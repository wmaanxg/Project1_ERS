package com.ers.controllers;

import com.ers.services.EmployeeService;
import com.ers.data.Employee;
import com.ers.data.Manager;
import com.ers.exceptions.InvalidOptionException;
import com.ers.exceptions.ForbiddenException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(description = "Manager view all employees servlet", urlPatterns = {"/view_all_employees"})
public class ViewAllEmployeesServlet extends HttpServlet {
    

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");
	
	if(account != null){
	    Manager mAccount = null;
	    try{
		mAccount = new Manager(account);
	    } catch(ForbiddenException e){
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		return;
	    }
	    ObjectMapper om = new ObjectMapper();
	    PrintWriter writer = response.getWriter();
	    List<Employee> employees = mAccount.getAllEmployees();
	    String strData = om.writeValueAsString(employees);
	    writer.write(strData);
	    response.setStatus(HttpServletResponse.SC_OK);
	    response.setHeader("content-type", "application/json");
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}
    }

}
