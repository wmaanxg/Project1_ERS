package com.ers.services;

import com.ers.data.dao.ReimbursementRequestDao;
import com.ers.data.ReimbursementRequest;
import com.ers.exceptions.InvalidOptionException;

import java.util.List;

public class ReimbursementRequestService{
    private ReimbursementRequestDao dao;

    public ReimbursementRequestService(){
	this.dao = new ReimbursementRequestDao();
    }

    public List<ReimbursementRequest> getAll(){
	return this.dao.getAll();
    }

    public ReimbursementRequest getOne(Integer id){
	return this.dao.getOne(id);
    }

}
