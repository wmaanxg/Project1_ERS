package com.ers.exceptions;

public class InvalidOptionException extends Exception{

    public InvalidOptionException(){
	super();
    }
    public InvalidOptionException(String message){
	super(message);
    }
}
