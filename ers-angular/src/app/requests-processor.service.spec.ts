import { TestBed, inject } from '@angular/core/testing';

import { RequestsProcessorService } from './requests-processor.service';

describe('RequestsProcessorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequestsProcessorService]
    });
  });

  it('should be created', inject([RequestsProcessorService], (service: RequestsProcessorService) => {
    expect(service).toBeTruthy();
  }));
});
