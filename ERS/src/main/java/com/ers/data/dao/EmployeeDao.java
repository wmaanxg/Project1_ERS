package com.ers.data.dao;

import com.ers.dbutils.ConnectionUtil;
import com.ers.data.Employee;
import com.ers.data.Employee.EmployeeBuilder;
import com.ers.exceptions.InvalidOptionException;
//import com.ers.data.Employee.Title;
//import com.ers.data.Employee.PasswordResetRequired;

import java.util.List;
import java.util.ArrayList;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Date;
import java.sql.Types;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class EmployeeDao implements ERSDao<Employee, Integer>{

    private static final Logger logger = LogManager.getLogger(EmployeeDao.class.getName());

    private static final String selectAllFields = "SELECT e.id, e.ssn, e.email, e.firstname, e.lastname, t.name, t.is_manager, p.name, e.manager_id, e.birthdate, e.phone, e.st_address, e.city, e.state, e.country, e.zipcode, e.password_hash FROM employee e JOIN employee_title t ON e.title_id = t.id JOIN password_reset_required p ON e.password_reset_required_id = p.id";

    /*
     * Gets all employees from the employee table and puts then into a list.
     *
     * @return A List of employee objects containing all employees in the table
     */
    @Override
    public List<Employee> getAll(){
	List<Employee> allEmployees = new ArrayList<>();
	
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields;
	    try(Statement s = c.createStatement();
		ResultSet results = s.executeQuery(sql);){
		
	    
		while(results.next()){
		    allEmployees.add(rowToEmployee(results));
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in EmployeeDao.getAll()");
	    logger.error(e.toString());
	}

	return allEmployees;
    }

    /*
     * Gets one employee from the employee table using its id.
     *
     * @param id An Integer corresponding the the primary key of the 
     * employee table
     * @return An employee object holding the data of the row selected
     */
    @Override
    public Employee getOne(Integer id){
	
        Employee emp = null;
	
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE e.id = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setInt(1, id);
		try(ResultSet results = ps.executeQuery()){	    
		    while(results.next()){
			emp = rowToEmployee(results);
		    }
		}
		c.commit();
	    
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in EmployeeDao.getOne()");
	    logger.error(e.toString());
	}

	return emp;
    }
    

    /*
     * Gets one employee from the employee table using its email.
     *
     * @param email A String corresponding the the email unique key of the 
     * employee table
     * @return An employee object holding the data of the row selected
     */
    public Employee getOne(String email){
	
        Employee emp = null;
	
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = selectAllFields + " WHERE e.email = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setString(1, email);
		try(ResultSet results = ps.executeQuery()){
		    while(results.next()){
			emp = rowToEmployee(results);
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in EmployeeDao.getOne()");
	    logger.error(e.toString());	    
	}

	return emp;
    }
    
    /*
     * Private helper function to convert a row from a ResultSet into an
     * Employee object.  Relies on the collumns being selected in a specific
     * Order
     *
     * @param row ResultSet pointing to the row that will be processed.
     * @return The Employee object generated from the given row.
     */
    private Employee rowToEmployee(ResultSet row) throws SQLException{
	EmployeeBuilder builder = new EmployeeBuilder(row.getInt(1),
						      row.getInt(2),
						      row.getString(3),
						      row.getString(4),
						      row.getString(5),
						      row.getString(6),
						      row.getString(7)
						      .equals("yes"),
						      row.getString(8))
	    .setManagerId(row.getInt(9))
	    .setBirthdate(row.getDate(10))
	    .setPhone(row.getString(11))
	    .setStAddress(row.getString(12))
	    .setCity(row.getString(13))
	    .setState(row.getString(14))
	    .setCountry(row.getString(15))
	    .setZipcode(row.getString(16))
	    .setPasswordHash(row.getString(17));
	    return builder.build();
    }


    /*
     * Inserts a new employee to the database.  Uses a pl/sql trigger to 
     * auto-increment the primary key.  ALWAYS ASSUMES THAT THE GIVEN 
     * EMPLOYEE IS A NEW RECORD. 
     *
     * @param e An Employee object holding the data to be inserted.
     */
    @Override
    public void saveNew(Employee e) throws InvalidOptionException{
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "INSERT INTO employee (ssn, email, firstname, lastname, title_id, password_reset_required_id, manager_id, birthdate, phone, st_address, city, state, country, zipcode, password_hash) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	   
	    try(PreparedStatement ps = c.prepareStatement(sql)){
	    
		ps.setInt(1, e.getSsn());
		ps.setString(2, e.getEmail());
		ps.setString(3, e.getFirstname());
		ps.setString(4, e.getLastname());
		ps.setInt(5, this.titleNameToId(e.getTitle()));
		ps.setInt(6, this.prrNameToId(e.getPasswordResetRequired()));
		if(e.getManagerId() == 0)
		    ps.setNull(7, Types.INTEGER);
		else
		    ps.setInt(7, e.getManagerId());
		ps.setDate(8, e.getBirthdate());
		ps.setString(9, e.getPhone());
		ps.setString(10, e.getStAddress());
		ps.setString(11, e.getCity());
		ps.setString(12, e.getState());
		ps.setString(13, e.getCountry());
		ps.setString(14, e.getZipcode());
		ps.setString(15, e.getPasswordHash());
	    
		ps.executeUpdate();
		c.commit();
	    } catch(SQLException ex){
		c.rollback();
		throw ex;
	    }
	} catch(SQLException ex){
	    logger.error("SQLException in EmployeeDao.saveNew()");
	    logger.error(ex.toString());
	}
    }


    /*
     * Updates an employee record using the data in the given Employee object.
     * Selects the record to be updated based on the id field of the object.
     *
     * @param e An Employee object holding the data to be updated.
     */
    @Override
    public void update(Employee e) throws InvalidOptionException{
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "UPDATE employee SET ssn = ?, email = ?, firstname = ?, lastname = ?, title_id = ?, password_reset_required_id = ?, manager_id = ?, birthdate = ?, phone = ?, st_address = ?, city = ?, state = ?, country = ?, zipcode = ?, password_hash = ? WHERE id = ?";
	   
	    try(PreparedStatement ps = c.prepareStatement(sql)){
	    
		ps.setInt(1, e.getSsn());
		ps.setString(2, e.getEmail());
		ps.setString(3, e.getFirstname());
		ps.setString(4, e.getLastname());
		ps.setInt(5, this.titleNameToId(e.getTitle()));
		ps.setInt(6, this.prrNameToId(e.getPasswordResetRequired()));
		if(e.getManagerId() == 0)
		    ps.setNull(7, Types.INTEGER);
		else
		    ps.setInt(7, e.getManagerId());
		ps.setDate(8, e.getBirthdate());
		ps.setString(9, e.getPhone());
		ps.setString(10, e.getStAddress());
		ps.setString(11, e.getCity());
		ps.setString(12, e.getState());
		ps.setString(13, e.getCountry());
		ps.setString(14, e.getZipcode());
		ps.setString(15, e.getPasswordHash());
	    
		ps.setInt(16, e.getId());
	    
		ps.executeUpdate();
		c.commit();
	    } catch(SQLException ex){
		c.rollback();
		throw ex;
	    }
	} catch(SQLException ex){
	    logger.error("SQLException in EmployeeDao.update()");
	    logger.error(ex.toString());
	}

    }

    public boolean emailExists(String email){
	boolean ret = true;
	
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "SELECT email FROM employee WHERE email = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setString(1, email);
		try(ResultSet rs = ps.executeQuery()){
		    ret = false;
		    while(rs.next()){
			ret = true;
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch (SQLException e){
	    logger.error("SQLException in EmployeeDao.emailExists()");
	    logger.error(e.toString());
	}

	return ret;
    }

    public Integer titleNameToId(String name) throws InvalidOptionException{
	Integer ret = null;
   
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "SELECT id FROM employee_title" +
		" WHERE name = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setString(1, name);
		try(ResultSet results = ps.executeQuery()){
		    while(results.next()){
			ret = results.getInt(1);
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch(SQLException e){
	    logger.error("SQLException in EmployeeDao.titleNametoId()");
	    logger.error(e.toString());
	}

	if(ret == null){
	    throw new InvalidOptionException("Not a valid employee title name.");
	}
	return ret;
    }
    
    // public String titleIdToName(Integer id) throws InvalidOptionException{
    // 	String ret = null;
   
    // 	try(Connection c = ConnectionUtil.newConnection()){
    // 	    String sql = "SELECT name FROM employee_title" +
    // 		" WHERE id = ?;";
    // 	    PreparedStatement ps = c.prepareStatement(sql);
    // 	    ps.setInt(1, id);
    // 	    ResultSet results = ps.executeQuery();

    // 	    while(results.next()){
    // 		ret = results.getString(1);
    // 	    }
	    
    // 	} catch(SQLException e){
    // 	    logger.error("SQLException in ReimbursementRequestDao.titleIdToName()");
    // 	    logger.error(e.toString());
    // 	}

    // 	if(ret == null){
    // 	    throw new InvalidOptionException("Not a valid employee title id.");
    // 	}
    // 	return ret;
    // }

    public Integer prrNameToId(String name) throws InvalidOptionException{
	Integer ret = null;
   
	try(Connection c = ConnectionUtil.newConnection()){
	    c.setAutoCommit(false);
	    String sql = "SELECT id FROM password_reset_required" +
		" WHERE name = ?";
	    try(PreparedStatement ps = c.prepareStatement(sql)){
		ps.setString(1, name);
		try(ResultSet results = ps.executeQuery()){
		    while(results.next()){
			ret = results.getInt(1);
		    }
		}
		c.commit();
	    } catch(SQLException e){
		c.rollback();
		throw e;
	    }
	} catch(SQLException e){
	    logger.error("SQLException in ReimbursementRequestDao.PRRNametoId()");
	    logger.error(e.toString());
	}

	if(ret == null){
	    throw new InvalidOptionException("Not a valid password reset required name.");
	}
	return ret;
    }
    
    // public String PRRIdToName(Integer id) throws InvalidOptionException{
    // 	String ret = null;
   
    // 	try(Connection c = ConnectionUtil.newConnection()){
    // 	    String sql = "SELECT name FROM password_reset_required" +
    // 		" WHERE id = ?;";
    // 	    PreparedStatement ps = c.prepareStatement(sql);
    // 	    ps.setInt(1, id);
    // 	    ResultSet results = ps.executeQuery();

    // 	    while(results.next()){
    // 		ret = results.getString(1);
    // 	    }
	    
    // 	} catch(SQLException e){
    // 	    logger.error("SQLException in ReimbursementRequestDao.PRRIdToName()");
    // 	    logger.error(e.toString());
    // 	}

    // 	if(ret == null){
    // 	    throw new InvalidOptionException("Not a valid password reset required id.");
    // 	}
    // 	return ret;
    // }


    
    /*
     * Deletes an employee based on a given Employee object.  
     * Selects for deletion based on the id field of the object. 
     *
     * @param e An Employee object holding id of the record to be deleted.
     */   
    // public void delete(Employee e){
    // 	this.delete(e.getId());
    // }

    /*
     * Delets an Employee record based on the id. 
     *
     * @param id The id corresponding to the id of the row to be deleted.
     */   
    // public void delete(Integer id){
    // 	try(Connection c = ConnectionUtil.newConnection()){
    // 	    String sql = "DELETE FROM employee WHERE id = ?";
    // 	    PreparedStatement ps = c.prepareStatement(sql);
    // 	    ps.setInt(1, id);
    // 	    ps.execute();
    // 	    c.commit();
    // 	} catch(SQLException e){
    // 	    logger.error("SQLException in EmployeeDao.delete()");
    // 	    logger.error(e.toString());
    // 	}
    // }
    

}
