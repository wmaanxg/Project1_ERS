package com.ers.controllers;

import com.ers.services.ReimbursementRequestService;
import com.ers.data.ReimbursementRequest;
import com.ers.data.Employee;
import com.ers.data.Manager;
import com.ers.exceptions.ForbiddenException;
import com.ers.exceptions.InvalidOptionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(description = "Resolve Request Servlet", urlPatterns = {"/resolve_request"})
public class ResolveRequestServlet extends HttpServlet {
    
    public static class ResolveStatus{
	public String status;
	public int id;
	public ResolveStatus(){}
    }
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	System.out.println(1);
	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");
	System.out.println(2);
	if(account != null){
	    Manager mAccount = null;
	    try{
		mAccount = new Manager(account);
	    } catch(ForbiddenException e){
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		return;
	    }
	    System.out.println(3);
	    ObjectMapper om = new ObjectMapper();
	    System.out.println(4);
	    try(BufferedReader reader = request.getReader();) {
		ResolveStatus resolveStatus = om.readValue(reader, ResolveStatus.class);
		ReimbursementRequestService service = new ReimbursementRequestService();
		ReimbursementRequest rRequest = service.getOne(resolveStatus.id);
		if(!rRequest.getStatus().equals("pending")){
		    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		    return;
		}
		
		String status = resolveStatus.status;
		System.out.println(status);
		switch(status){
		case "approve":
		    mAccount.approveRequest(rRequest);
		    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		    response.setHeader("content-type", "text/plain");
		    break;
		case "deny":
		    mAccount.denyRequest(rRequest);
		    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		    response.setHeader("content-type", "text/plain");
		    break;
		default:
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    break;
		}
		System.out.println(6);
		
	    } catch(InvalidOptionException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(JsonProcessingException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(ArithmeticException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(InputMismatchException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(NoSuchElementException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    }
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}
	System.out.println(7);
    }

}
