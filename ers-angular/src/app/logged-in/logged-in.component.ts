import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

import {Employee} from '../domain/employee';

@Component({
  selector: 'app-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoggedInComponent implements OnInit {

    account: Employee;
    constructor(private router: Router,
		private route: ActivatedRoute,
		private http: HttpClient) { }

    ngOnInit() {
	this.getAccount();
    }

    onSubmit(){
	this.http.post('http://localhost:8080/ERS/update_info', this.account, {withCredentials: true})
	    .subscribe(
		data =>{
		    this.getAccount();
		},
		err => {
		    this.router.navigateByUrl('/');
		});
    }

    toSubmitRequest(){
	this.router.navigate(["../submit_request"], {relativeTo: this.route});
    }

    toViewOwnRequests(){
	this.router.navigate(["../view_own_requests"], {relativeTo: this.route});
    }
    toViewOthersRequests(){
	this.router.navigate(["../view_others_requests"], {relativeTo: this.route});
    }
    toViewEmployees(){
	this.router.navigate(["../view_employees"], {relativeTo: this.route});
    }
    
    getAccount() {
	this.http.get<Employee>('http://localhost:8080/ERS/account_info', {withCredentials: true})
	    .subscribe(
		data =>{
		    console.log(JSON.stringify(data.birthdate));
		    this.account = data;
		},
		err => {
		    this.router.navigateByUrl('/');
		});	
    }

}
