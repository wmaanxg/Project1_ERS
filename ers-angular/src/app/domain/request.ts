export class Request{
    id: number;
    submitterId: number;
    timeSubmitted: string;
    amount: number;
    'type': string;
    status: string;
    receipt: string;
    resolverId: number;
}
