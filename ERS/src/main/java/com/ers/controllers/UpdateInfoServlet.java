package com.ers.controllers;

import com.ers.services.EmployeeService;
import com.ers.data.Employee;
import com.ers.data.Manager;
import com.ers.exceptions.ForbiddenException;
import com.ers.exceptions.InvalidOptionException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.io.BufferedReader;
import java.sql.Date;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(description = "Update Account Info Servlet", urlPatterns = {"/update_info"})
public class UpdateInfoServlet extends HttpServlet {
    
    public static class EmployeeJson{
	public int id;
	public int ssn;
	public String email;
	public String firstname;
	public String lastname;
	public String title;
	public boolean isManager;
	public int managerId;
	public String birthdate;
	public String phone;
	public String stAddress;
	public String city;
	public String state;
	public String country;
	public String zipcode;

	public EmployeeJson(){}
    }
    /**
     * @see HttpServlet#doPut(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	HttpSession session = request.getSession();
	Employee account = (Employee) session.getAttribute("account");

	if(account != null){
	    ObjectMapper om = new ObjectMapper();

	    try(BufferedReader reader = request.getReader();) {
		EmployeeJson updatedInfo = om.readValue(reader, EmployeeJson.class);
		EmployeeService service = new EmployeeService();
		if((!updatedInfo.email.equals(account.getEmail())) &&
		   service.emailExists(updatedInfo.email)){
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    return;
		}

		//account.setSsn(updatedInfo.ssn);
		account.setEmail(updatedInfo.email);
		//account.setFirstname(updatedInfo.firstname);
		//account.setLastname(updatedInfo.lastname);
		account.setBirthdate(updatedInfo.birthdate == null ? null : Date.valueOf(updatedInfo.birthdate));
		account.setPhone(updatedInfo.phone);
		account.setStAddress(updatedInfo.stAddress);
		account.setCity(updatedInfo.city);
		account.setState(updatedInfo.state);
		account.setCountry(updatedInfo.country);
		account.setZipcode(updatedInfo.zipcode);
		    
		service.update(account);
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	    } catch(InvalidOptionException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } catch(JsonProcessingException e){
		System.out.println(e);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } 
	}
	else{
	    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	    response.setHeader("WWW-Authenticate", "html-form");
	}

    }

}
