import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router'
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-submit-request',
  templateUrl: './submit-request.component.html',
  styleUrls: ['./submit-request.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SubmitRequestComponent implements OnInit {

    amount: string;
    rType: string;
    error: string;
    
    constructor(private route: ActivatedRoute,
		private router: Router,
		private http: HttpClient) { }
    
    ngOnInit() {
	this.http.get('http://localhost:8080/ERS/submit_request', {withCredentials: true})
	    .subscribe(
		data =>{
		    //noop
		},
		err => {
		    this.router.navigateByUrl('/');
		});
    }

    onSubmit(){
	var submissionObj = {'amount': this.amount, 'type': this.rType};
	this.http.post('http://localhost:8080/ERS/submit_request', submissionObj, {responseType: 'text', withCredentials: true})
	    .subscribe(
		data =>{
		    this.router.navigate(['../logged_in'], {relativeTo: this.route});
		},
		err => {
		    this.error = "Invalid request submission";  
		});
	
    }
}
